# Note: this file is not included in the upstream stubs package, because that project
# uses Poetry instead of setuptools; we include this for convenience.

import os

from setuptools import setup, find_packages

name = "discord-stubs"


def find_stub_files():
    result = []
    for root, _dirs, files in os.walk(name):
        for file in files:
            if file.endswith(".pyi"):
                if os.path.sep in root:
                    sub_root = root.split(os.path.sep, 1)[-1]
                    file = os.path.join(sub_root, file)
                result.append(file)
    return result


setup(
    name=name,
    url="https://gitlab.com/lgbtq-discord/unibot",
    version="0.0.0",
    maintainer="LGBTQ+Discord Staff",
    packages=find_packages(),
    license="MIT",
    description="Type stubs for discord.py",
    include_package_data=True,
    package_data={name: find_stub_files()},
    python_requires=">=3.7",
)
