Easy way to run Prisma as a systemd service. Hardcoded to use `/home/prisma/production/prisma` on the Linode server. The bot will automatically restart on failures and log to the system journal.
/home/prisma/.local/bin
## One time setup (as root)
`# loginctl enable-linger prisma`
https://wiki.archlinux.org/title/systemd/User#Automatic_start-up_of_systemd_user_instances
## Setup
2. `# sudo cp prisma.service /etc/systemd/system`
4. `# sudo systemctl enable prisma.service`
5. `# sudo systemctl start prisma.service`
## Logging
Logs should now be available when running `journalctl -u prisma.service` when signed in as the Prisma user or through Grafana.
## Caveats
- This doesn't start up Postgres and is meant only as a stop-gap until we get proper Docker compose support working for the bot.
- Therefore, if you try and start the service without the DB being up...you may get catastrophic failures!