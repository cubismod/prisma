"""create role table

Revision ID: bc57396a6501
Revises: 79d2d5df4386
Create Date: 2021-05-20 20:31:46.722675

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "bc57396a6501"
down_revision = "79d2d5df4386"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'roles',
        sa.Column('rankname', sa.Text, nullable=False),
        sa.Column('rankid', sa.BigInteger, nullable=False, primary_key=True),
        sa.Column('joinable', sa.Boolean, nullable=False),
        sa.Column('pingable', sa.Boolean, nullable=False)
        )

def downgrade():
    op.drop_table("roles")
