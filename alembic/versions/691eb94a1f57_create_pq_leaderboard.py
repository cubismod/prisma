"""create pq leaderboard

Revision ID: 691eb94a1f57
Revises: 7d58fb76c39f
Create Date: 2022-01-19 21:48:00.860358

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "691eb94a1f57"
down_revision = "7d58fb76c39f"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'pqleaderboard',
        sa.Column('user', sa.BigInteger, nullable=False, primary_key=True),
        sa.Column('sessionscore', sa.Integer, nullable=False),
        sa.Column('seasonscore', sa.Integer, nullable=False)
    )
    


def downgrade():
    op.drop_table("pqleaderboard")
