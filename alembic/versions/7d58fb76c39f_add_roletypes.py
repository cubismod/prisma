"""add roletypes

Revision ID: 7d58fb76c39f
Revises: bc57396a6501
Create Date: 2021-06-30 23:57:14.807259

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "7d58fb76c39f"
down_revision = "bc57396a6501"
branch_labels = None
depends_on = None

role_type = postgresql.ENUM("AGE", "GENDER", "SEXUALITY", "ROMANTIC", "PRONOUN", "COUNTRY", name="role_type")

def upgrade():
    role_type.create(op.get_bind())
    op.add_column("roles", sa.Column("type", role_type, nullable=True,))

def downgrade():
    op.drop_column("roles", "type")
    role_type.drop(op.get_bind())
