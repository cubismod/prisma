import discord
from discord.ext import commands

from prisma import Context, UserError, Settings, db, style
from prisma.cogs import Cog, make_cog_setup
from prisma.util import is_staff, is_perm_staff, is_lgbtq_server
from prisma.bot import Config


from pydantic import BaseModel

import sqlalchemy as sa
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

import toml

class Invite(db.Model):

    __tablename__ = "invites"
    origin = sa.Column(sa.Unicode, nullable = False)
    uses = sa.Column(sa.Integer, nullable = False)
    inviteString = sa.Column(sa.Unicode, nullable = False,  primary_key = True)


class VerificationCog(Cog, name="Verification"):
    class Config(BaseModel):
        helper_channel_id: int
        sniper_id: int
    
    @commands.command()
    @commands.check(is_staff)
    @commands.check(is_lgbtq_server)
    async def verify(self, ctx: Context, member: discord.Member):
        verified_role = ctx.guild.get_role(self.bot.config.roles.user.verified)
        general = ctx.guild.get_channel(331517548636143626)

        # WELCOME MESSAGE AND CALLER DM #
        welcome_message = f"""
    **Welcome, {member.mention}!**
    • Please make sure to read #info 
    • To set your profile tags, send <@843706833108598804> a DM with the command *`p!info`* 
    • Introduce yourself on #bio if you wish 
    • Check out our opt-in channels with the command *`p!join`* in #bot!! 
    • Feel free to start a conversation about your favourite thing in the appropriate Topical channel further down the sidebar! 
      We appreciate #general can be intimidating at first, don't worry :rainbow_heart:    
    :42a:
        """
        dm_message = f"""
    ```
    {welcome_message}```
        """
        mention_message = f"""
    {general.mention} {general.mention} {general.mention} {general.mention} {general.mention} {general.mention}
        """
        ###################################

        if verified_role in member.roles:
            raise UserError("That member is already verified!")

        try:
            await member.add_roles(verified_role, reason="Verifying member")
            # Only message author if sucessfully added role
            await ctx.author.send(dm_message)
            await ctx.author.send(mention_message)
        except discord.errors.Forbidden:
            raise UserError(
                "I have insufficient permissions to verify!",
                f"This might be because my role is below {verified_role.mention} in the "
                f"role list.",
            )
        except Exception as e:
            await ctx.author.send('Something went wrong...try again!')
            print(repr(e))

        
    @commands.command()
    @commands.check(is_perm_staff)
    @commands.check(is_lgbtq_server)
    async def changeorigin(self, ctx: Context, code: str, *, origin: str):
        ##gets all invites in the database
        invites_db = ctx.db.query(Invite).all()

        for inv in invites_db:
            if inv.inviteString == code:
                inv.origin = origin
        ctx.db.commit()


    @commands.command()
    @commands.check(is_staff)
    @commands.check(is_lgbtq_server)
    async def invites(self, ctx: Context):
        ##requests all invites from the guild 
        invites = await ctx.guild.invites()

        ##gets all invites in the database
        invites_db = ctx.db.query(Invite).all()
        inviteEmbed = discord.Embed(title = f" {style.icons.INFO} Tracked Invites")
        inviteEmbed.colour = style.colours.BLUE
        for inv in invites_db:
            inviteEmbed.add_field(
                    inline=False,
                    name=f"**{inv.inviteString}**",
                    value=f"**Origin**: {inv.origin}"
                    f"\n **Uses**: {inv.uses}",
                )
        if len(invites_db) > 0:
            await ctx.send(embed=inviteEmbed)
        else:
            await ctx.error(title = "Error Listing Invites", description = "No invites found")

    @commands.command()
    @commands.check(is_staff)
    @commands.check(is_lgbtq_server)
    async def removeinvite(self, ctx: Context, code: str):
        ##gets all invites in the database
        removed_inv = None
        invites_db = ctx.db.query(Invite).all()
        for inv in invites_db:
            if inv.inviteString == code:
                removed_inv = inv
                ctx.db.delete(inv)
                continue
        ctx.db.commit()

        if removed_inv == None:
            await ctx.error(title = "Error Removing Invite", description = f" Invite {code} not found")
        else:
            embed = discord.Embed(title = f"{style.icons.INFO} Removed Invite from Database")
            embed.add_field(
                inline = False,
                name = f"Code: {inv.inviteString}",
                value = (f"**Invite Origin**: {inv.origin} "
                f"\n **Uses**: {inv.uses}")
            )
            await ctx.send(embed=embed)




    @commands.command()
    @commands.check(is_staff)
    @commands.check(is_lgbtq_server)
    async def syncinvites(self, ctx: Context):
        ##requests all invites from the guild 
        invites = await ctx.guild.invites()

        ##gets all invites in the database
        invites_db = ctx.db.query(Invite).all()
        updateList = []
        addList = []
        found = 0
        for inv in invites:
            found = 0 
            for db_inv in invites_db:
                if (db_inv.inviteString == inv.code):
                    db_inv.uses = inv.uses
                    found = 1
                    updateList.append(db_inv)
                    continue
            if (found == 0 ):
                new_inv = Invite(origin = "unknown", uses = inv.uses, inviteString = inv.code)
                ctx.db.add(new_inv)
                addList.append(new_inv)
        ctx.db.commit()
        updateEmbed = discord.Embed(title = f" {style.icons.INFO} Updated Invites")
        updateEmbed.colour = style.colours.BLUE
        addEmbed = discord.Embed(title = f"{style.icons.INFO} Added Invites")
        addEmbed.colour = style.colours.BLUE
        for inv in updateList:
            updateEmbed.add_field(
                    inline=False,
                    name=f"**{inv.inviteString}**",
                    value=f"**Origin**: {inv.origin}"
                    f"\n **Uses**: {inv.uses}",
                )
        for inv in addList:
            addEmbed.add_field(
                    inline=False,
                    name=f"**{inv.inviteString}**",
                    value=f"**Origin**: {inv.origin}"
                    f"\n **Uses**: {inv.uses}",
                )

        if (len(updateList) > 0):
            await ctx.send(embed = updateEmbed)
        if (len(addList) > 0):
            await ctx.send(embed = addEmbed)
        if (len(addList) < 1 and len(updateList) < 1):
            await ctx.error(title = "Error Adding/Updating Invites", description = "No invites found to be added or updated")




    @commands.Cog.listener()
    async def on_member_join(self, member):
        ##requests all invites from the guild 
        invites = await member.guild.invites()

        ##loads environment variables         
        settings = Settings()
        ##builds the query engine
        full_url = sa.engine.url.make_url(settings.DB_URL)
        full_url = full_url.set(password=settings.DB_PASSWORD.get_secret_value())
        db_engine = create_engine(full_url)

        ##builds a db session from the engine
        Session = sessionmaker(bind=db_engine)
        session = Session()

        ##gets all invites in the database
        invites_db = session.query(Invite).all()
        usedInvite = None
        ##loops through and compares each invite to see if the number has changed from what was recorded
        for inv in invites:
            for db_inv in invites_db :
                if inv.code == db_inv.inviteString:
                    print(inv.code)
                    print(inv.uses)
                    print(db_inv.uses)
                    ##if an invite has one more use than what is recorded, we take that to be the used invite
                    if inv.uses - 1 == db_inv.uses:
                        usedInvite = db_inv
                        db_inv.uses = inv.uses 
        session.commit()
        ##if it didn't find an invite that was different, returns an error in the helper ping



        log_channel = member.guild.get_channel(self.config.helper_channel_id)
        embed = discord.Embed()
        embed.colour = style.colours.BLUE
        embed.set_thumbnail(url=member.avatar_url)
        if usedInvite == None:
            embed.description = (f"{style.icons.INFO} {member.mention} **joined the server.**"
            f" \n **Username**: {member.name}"
            f" \n**Joined On**: {member.joined_at: %d %B, %Y} at {member.joined_at: %I:%S %p}"
            f"\n **ID**: {member.id} \n**Account Created**: {member.created_at: %d %B, %Y} at {member.created_at:  %I:%S %p} ( {(member.joined_at - member.created_at).days} days ago)"
            f"\n\n**Invite not Found, please run p!syncinvites**")

        else:
            embed.description = (f"{style.icons.INFO} {member.mention} **joined the server.**"
            f" \n **Username**: {member.name}"
            f" \n**Joined On**: {member.joined_at: %d %B, %Y} at {member.joined_at: %I:%S %p}"
            f"\n **ID**: {member.id} \n**Account Created**: {member.created_at: %d %B, %Y} at {member.created_at:  %I:%S %p} ( {(member.joined_at - member.created_at).days} days ago)"
            f"\n\n**Invite Used**: {usedInvite.inviteString}"
            f"\n **Invite Origin**: {usedInvite.origin}"
            f"\n **Invite Uses**: {usedInvite.uses} ")
  

        snipe_mention = member.guild.get_role(self.config.sniper_id).mention   

     

        await log_channel.send(snipe_mention,embed=embed)

    @commands.Cog.listener()
    async def on_member_remove(self,member):
        settings = Settings()
        config_dict = toml.load(settings.CONFIG_PATH)
        cnfg = Config.parse_obj(config_dict)
        log_channel = member.guild.get_channel(self.config.helper_channel_id)
        verified_role = member.guild.get_role(cnfg.roles.user.verified)
        if verified_role not in member.roles:
            embed = discord.Embed(title = f"{style.icons.ERROR} User {member.mention} ({member}) left before verification")
            embed.colour = style.colours.RED
            await log_channel.send(embed=embed)

setup = make_cog_setup(VerificationCog)
