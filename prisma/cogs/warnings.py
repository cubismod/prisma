from __future__ import annotations

from datetime import datetime
from enum import Enum
from itertools import groupby
from operator import attrgetter

import discord
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from discord.ext import commands
from pydantic import BaseModel
from sqlalchemy.ext.hybrid import hybrid_method, hybrid_property

import prisma.db as db
from prisma import Context, style
from prisma.cogs import Cog, make_cog_setup
from prisma.util import FriendlyTimeDelta, mention_fallback, is_staff, is_perm_staff, attachments_to_str


class WarningKind(Enum):
    FORMAL = "formal"
    VERBAL = "verbal"
    DBATC = "dbatc"

    @property
    def adjective(self) -> str:
        if self == WarningKind.DBATC:
            return "DBATC"

        return self.value
        
    @property
    def adverb(self) -> str:
        """
        Give the adverb used with this type of warn, e.g. the "verbally" in "User has
        been verbally warned."
        """

        if self == WarningKind.FORMAL:
            return "formally"
        elif self == WarningKind.VERBAL:
            return "verbally"
        elif self == WarningKind.DBATC:
            return "DBATC"

        raise ValueError(f"unknown enum variant {self}")


class Warning(db.Model):
    __tablename__ = "warnings"

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.BigInteger, nullable=False)
    kind = sa.Column(pg.ENUM(WarningKind, name="warning_kind"), nullable=False)
    reason = sa.Column(sa.String, nullable=False)
    issued_at = sa.Column(sa.DateTime, nullable=False, default=sa.sql.func.now())
    issuer_id = sa.Column(sa.BigInteger, nullable=False)
    duration = sa.Column(sa.Interval, nullable=False)

    @hybrid_property
    def expiration_date(self) -> datetime:
        return self.issued_at + self.duration

    @hybrid_method
    def expired(self, as_of: datetime) -> bool:
        return as_of > self.expiration_date

    @expired.expression
    def expired(cls, as_of):
        return as_of > cls.expiration_date

# remember this is a class, so you'll need to access its member functions with self.function_name() instead of just function_name()
class WarningsCog(Cog, name="Warnings"):
    class Config(BaseModel):
        log_channel_id: int
        default_duration: FriendlyTimeDelta



    # formal warning command, DMs user as well. 
    @commands.command()
    @commands.check(is_staff)
    async def warn(self, ctx: Context, member: discord.Member, *, reason: str):
        """Send and log a formal warning."""
        
        # add in image embed to the reason if one exists
        reason = reason + ' ' + attachments_to_str(ctx)

        await self.log_warning(ctx, WarningKind.FORMAL, member, reason)
        await self.send_warning_dm(ctx, member, reason)

        await ctx.success(
            f"{mention_fallback(member)} has been warned by {ctx.author.mention}.",
            title_mode="description",
        )

    # this should be able to have functionality as a command as well as a group, but refuses to work for some reason.
    # Don't add functionality leave as pass    
    @commands.group()
    async def warns(self, ctx: Context):
        pass

    #clears ALL warnings for a user
    @warns.command()
    @commands.check(is_perm_staff)
    async def clear(self, ctx: Context, user: discord.User):
        """Clear all warnings for a user."""
        deleted = ctx.db.query(Warning).filter(Warning.user_id == user.id).delete()

        if deleted == 0:
            await ctx.warning(
                f"{mention_fallback(user)} has no warnings to clear.",
                title_mode="description",
            )
        else:
            await ctx.success(
                f"Cleared {deleted} warnings for {mention_fallback(user)}.",
                title_mode="description",
            )
    
    ##removes a single warning
    @warns.command()
    @commands.check(is_staff)
    async def remove(self, ctx: Context, warning_id: int):
        """Remove a single warning by its ID."""
        # TODO: consider taking a list of warning IDs

        warning = ctx.db.query(Warning).get(warning_id)

        if warning is None:
            await ctx.error(
                f"Could not find warning with ID {warning_id}.",
                description="No warnings were removed.",
            )
            return

        ctx.db.delete(warning)

        await ctx.success("Removed 1 warning.")

    ##issue a death by a thousand cuts warning
    @commands.command()
    @commands.check(is_staff)
    async def dbatc(self, ctx: Context, member: discord.Member, *, reason:str):
        reason = reason + ' ' + attachments_to_str(ctx)
        await self.log_warning(ctx, WarningKind.DBATC, member, reason)
        await self.send_warning_dm(ctx, member, reason)
        
    ##issue a verbal warning (does not DM)
    @commands.command()
    @commands.check(is_staff)
    async def verbal(self, ctx: Context, member: discord.Member, *,  reason: str):
        reason = reason + ' ' + attachments_to_str(ctx)
        await self.log(ctx, WarningKind.VERBAL, member, reason)

    # lists all warnings for a user
    @commands.command()
    @commands.check(is_staff)
    async def warnings(self, ctx: Context, user: discord.User):
        
        warnings = (
            ctx.db.query(Warning)
            .filter(Warning.user_id == user.id)
            .filter(
                ~(
                    (Warning.kind == WarningKind.FORMAL)
                    & Warning.expired(sa.sql.func.now())
                )
            )
            .order_by(Warning.kind)
            .order_by(Warning.issued_at)
        )

        noWarnings = True

        for kind, group in groupby(warnings, key=attrgetter("kind")):
            warnings = list(group)
            if len(warnings) == 0:
                continue
            else:
                noWarnings = False

            embed = style.info_embed(
                f"{kind.adjective} warnings for {mention_fallback(user)}",
                title_mode="description",
            )

            for warning in warnings:
                issuer = ctx.bot.get_user(warning.issuer_id)

                embed.add_field(
                    inline=False,
                    name=f"**{warning.issued_at.isoformat()}**",
                    value=f"**Warning ID {warning.id}"
                    f" issued by {mention_fallback(issuer)}**"
                    f"\n{warning.reason}",
                )

            await ctx.send(embed=embed)

        if noWarnings:
            embed = style.error_embed(
                f"No warnings found",
                title_mode="description",
                description=(
                    f"No warnings found for {mention_fallback(user)}"
                ),
            )
            await ctx.send(embed=embed)

    async def log(self, ctx: Context, kind: WarningKind, member: discord.Member, reason: str):
        """Log a warning (usually a verbal one) without DMing the warned user."""

        await self.log_warning(ctx, kind, member, reason)

        await ctx.success(
            f"Logging a {kind.adjective} warn for {mention_fallback(member)})",
            title_mode="description",
        )

    async def log_warning(
        self, ctx: Context, kind: WarningKind, member: discord.Member, reason: str
    ) -> None:
        """Write a warning to the DB and to the log channel for warnings."""

        warning = Warning(
            user_id=member.id,
            issuer_id=ctx.author.id,
            kind=kind,
            reason=reason,
            # TODO: make verbal & DBATC warnings not expire
            duration=self.config.default_duration,
        )

        ctx.db.add(warning)
        ctx.db.flush()

        log_embed = style.warning_embed(
            f"{mention_fallback(member)} has been {kind.adverb} warned"
            f" by {ctx.author.mention}.",
            description=(
                f"**{warning.issued_at.isoformat()}**\n**Warning ID: {warning.id}**"
            ),
            title_mode="description",
        )
        log_embed.add_field(name="Reason", value=reason)

        log_channel = ctx.guild.get_channel(self.config.log_channel_id)
        await log_channel.send(embed=log_embed)

    async def send_warning_dm(
        self, ctx: Context, member: discord.Member, reason: str
    ) -> None:
        embed = style.warning_embed(
            f"You have been warned by {mention_fallback(ctx.author)} on {ctx.guild}.",
            title_mode="description",
        )
        embed.add_field(name="Reason", value=reason)

        try:
            await member.send(embed=embed)
        except discord.Forbidden:
            embed = style.error_embed(
                f"Could not send a DM to {member.mention} ({member})",
                title_mode="description",
                description=(
                    "They're not accepting DMs from this server,"
                    " or they have blocked me."
                ),
            )


setup = make_cog_setup(WarningsCog)
