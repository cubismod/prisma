from datetime import datetime, timedelta

import hypothesis.strategies as st
from hypothesis import assume, given

from ..warnings import Warning

warnings = st.builds(
    Warning,
    id=st.integers(min_value=0),
    user_id=st.integers(min_value=0),
    reason=st.text(),
    issued_at=st.datetimes(),
    duration=st.timedeltas(),
)


class TestWarning:
    @given(
        issued_at=st.datetimes(),
        duration=st.timedeltas(
            min_value=timedelta.resolution, max_value=timedelta(weeks=52)
        ),
    )
    def test_expiration_date_after_issued(self, issued_at, duration):
        warning = Warning(issued_at=issued_at, duration=duration)

        try:
            assert warning.expiration_date > warning.issued_at
        except OverflowError:
            # Overflow is unlikely to occur in the real world; when it does, just failing
            # is fine.
            assume(False)

    def test_expired_true(self):
        issued = datetime(1984, 1, 1)
        now = datetime(1984, 1, 15)
        warning = Warning(issued_at=issued, duration=timedelta(days=10))

        assert warning.expired(as_of=now)

    def test_expired_false(self):
        issued = datetime(1984, 1, 1)
        now = datetime(1984, 1, 2)
        warning = Warning(issued_at=issued, duration=timedelta(days=10))

        assert not warning.expired(as_of=now)
