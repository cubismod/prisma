FROM python:3.8

ENV PYTHONUNBUFFERED 1
RUN pip install pipenv

WORKDIR /code

# First we only copy the `Pipfile{.lock}`, since that's all we need to install
# dependencies...
COPY Pipfile Pipfile.lock ./
# ...except since we're installing the main package and discord.py stubs locally, we also
# need the `setup.py`s for that. It's OK that the actual source files aren't copied over
# yet, since the packages are installed as editable.
COPY setup.py ./
COPY discord-stubs/setup.py discord-stubs/

# TODO: this produces an image meant for developing and testing; for a production image,
# we don't want --dev, nor do we need pipenv installed. So we want a separate, multistage
# Dockerfile for that.
RUN pipenv install --deploy --system --dev

COPY . .

CMD python -m prisma

