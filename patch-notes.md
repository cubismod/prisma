# Prisma

Patch Notes for Prisma!

## 0.1 - Initial Release - April 27, 2021 (Iso, Valtari, Reltor)
- Added Warn commands
- Added Ban command

## 0.2 - Warnings Rework - May 10, 2021 (Reltor)
- Replaced p!warnings list with p!warnings
- Replaced p!warnings log with p!verbal and p!dbatc
- Replaced p!warnings remove with p!warns remove
- Replaced p!warnings clear with p!warns clear

## 0.3 - Join Notifications - May 15, 2021 (Reltor)
- Prisma now posts in #helpers when a user joins

## 0.3.1 - Invite Tracking - May 16, 2021 (Reltor)
- Prisma now identifies which invite a user joined with
- Added p!changeorigin to edit where a particular invite is posted
- Added p!removeinvite to remove an invite from tracking
- Added p!invites to list all tracked invites
- Added p!syncinvites to update use counts of all invites, and add untracked invites to tracking

## 0.4 - User Role Assignment - May 18, 2021 (Aardvark)
- Added p!join for a user to assign themself a joinable role

## 0.4.1 - User Role Mention and Warning Update (Aardvark and Cubis)
- Added p!mention for a user to mention a mentionable role (Aardvark)
- Updated the warning commands to accept an uploaded picture (Cubis)